<?php


for ($i = 0; $i < 20; $i++) {
    $unsortedArray[] = rand(0, 10);
}

echo "Initial unsorted array: \n";
echo implode(', ', $unsortedArray) . "\n";

echo "Sorted array: \n";
echo implode(', ', insertSort($unsortedArray)) . "\n";

function insertSort(array $sortedArray): array
{
    $countSortedArray = count($sortedArray);

    for ($i = 1; $i < $countSortedArray; $i++) {
        $currentValue = $sortedArray[$i];
        $j = $i - 1;

        while ($j >= 0 && $sortedArray[$j] > $currentValue) {
            $sortedArray[$j + 1] = $sortedArray[$j];
            $j--;
        }

        $sortedArray[$j + 1] = $currentValue;
    }

    return $sortedArray;
}
